require 'securerandom'
require 'open3'
require 'shellwords'

class String
  def is_integer?
    self.to_i.to_s == self
  end
end

$LETTERS = (65..90).to_a + (97..122).to_a
$NUMBERS = (48..57).to_a
$SYMBOLS = ['~','!','@','#','$','%','^','&','*','_','-','+','=','`','|',"\\",'(',')','{','}','[',']',':',';','<','>','.','?','/'].map{|x| x.ord}
$CHRS = $LETTERS+$NUMBERS+$SYMBOLS

$GOLD = 1.61803398874989484820458683436563811772030917980576286213544862270526046281890244970720720418939113748475408807538689175212663386222353693179318006076672635443338908659593958290563832266131992829026788067520876689250171169620703222104
$EUL_MAS = 0.57721566490153286060651209008240243104215933593992359880576723488486772677766467093694706329174674951463144724980708248096050401448654283622417399764492353625350033374293733773767394279259525824709491600873520394816567085323315177661152

def windowsescape(str)
	new_str = ""
	str.each_char do |c|
		new_str += "^"+c
	end
	return new_str
end

def gen_pass(len)
	pass = ""
	randombytes = SecureRandom.hex(len*2)
	len.times do
		byte = 0
		loop do
			$CHRS.shuffle!
			if randombytes == ""
				randombytes = SecureRandom.hex(len*2)
			end
			byte = randombytes[0..1].to_i(16)
			randombytes = randombytes[2...randombytes.length]
			break if byte < $CHRS.length
		end

		pi_twist = ((Math::E**$GOLD)*Math::PI)**(Random.rand*(2+$EUL_MAS))

		byte += pi_twist
		if pi_twist.to_i.even?
			byte += ((Math::PI**$EUL_MAS)*Math::E)**(Random.rand*(2+$GOLD))
		end

		byte = byte.to_i
		byte %= $CHRS.length

		byte += Time.now.to_i**Random.rand

		byte = byte.to_i
		byte %= $CHRS.length

		pass += $CHRS[byte].chr
	end
	return pass
end

if ARGV[0].nil?
	len = 30+Random.rand(-4..5)
else
	len = ARGV[0].to_i
end

if ARGV[1] == 'symoff'
	$CHRS = $LETTERS+$NUMBERS
end

pass = gen_pass(len)
puts pass
Open3.pipeline("echo #{windowsescape(pass)}", "clip")

if __FILE__ == $0
	inp = gets.chomp

	loop do
		if inp == 'symoff'
			$CHRS = $LETTERS+$NUMBERS
			puts "Symbols disabled."
		elsif inp == 'symon'
			$CHRS = $LETTERS+$NUMBERS+$SYMBOLS
			puts "Symbols enabled."
		elsif inp.is_integer?
			pass = gen_pass(inp.to_i)
			puts pass
			Open3.pipeline("echo #{windowsescape(pass)}", "clip")
		elsif inp == 'end' || inp == 'exit'
			break
		elsif inp == ''
			pass = gen_pass(30+Random.rand(-4..5))
			puts pass
			Open3.pipeline("echo #{windowsescape(pass)}", "clip")
		elsif ['help', 'h'].include? inp
			puts "Commands: symoff, symon, [some_number], exit"
		else
			puts "I didn't understand your command."
		end
		inp = gets.chomp
	end
end
