# Password-Generator-windows
Password Generator for Windows using Ruby. Uses mix of magic constants, time, mersenne twister and Window's own SecureRandom to generate passwords.

To run, simply run `ruby ./pass_creator.rb`
This will generate a password that is displayed and copied to the clipboard. After this, you can type in commands to alter options for other passwords you want to generate.

* "h" or "help" - shows all the commands
* "symoff" - disable use of symbols in password.
* "symon" - enable use of symbols in password.
* "[n]" - typing in an integer will give you a password of n length
* "end" or "exit" - exit the program

![terminal.png](https://i.imgur.com/pg8CNYg.png?2)
